
//
//  TVViewController.swift
//  TableView
//
//  Created by Dhanushkumar Kanagaraj on 15/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit
import iOSDropDown

class TVViewController: UIViewController {

    // MARK: - View life cycle methods
                                
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
    }
    
    // MARK: - Observer methods

    func addObserver() {
        // Add notification observers
    }

    func removeObserver() {
        // Remove notification observers
    }
    
    // MARK: - Register Nib method

    func registerNib() {
        // Regiater nib goes here
    }

}
