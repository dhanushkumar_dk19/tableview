//
//  TVCollectionViewCell.swift
//  TableView
//
//  Created by Dhanushkumar Kanagaraj on 15/12/20.
//  Copyright © 2020 Dhanushkumar Kanagaraj. All rights reserved.
//

import UIKit

class TVCollectionViewCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        customise()
    }
    
    // MARK: - Custom methods
    
    func customise() {
        // Customizing UI goes here
    }
    
}
